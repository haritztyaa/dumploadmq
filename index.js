const config = require('config');
var stompit = require('stompit');
var log = require('./helper/logger');
var db = require('./helper/db');

async function main (){
    const arg = process.argv
    let option = {
        command : '',
        file : null,
        init : false,
        test : false,
        help : false
    }
    for(var i = 0; i<arg.length; i++){
        if(arg[i]=='--cmd') option.command = arg[i+1];
        if(arg[i]=='--file') option.file = arg[i+1];
        if(arg[i]=='--init') option.init = true;
        if(arg[i]=='--test') option.test = true;
        if(arg[i]=='--help') option.help = true;
    }
    if(helpvalidate(option,arg)==false) process.exit();
    var dbhelper = new db.DbHelper(option.file);
    //jika init
    if (option.init) dbhelper.init();

    //jika test
    if(option.test){
        // test database
        try{
            await dbhelper.test();
        } catch(e){
            log.warn(e)
            process.exit();
        }
    }
    

    //jika dump
    if(option.command=='dump') dump(dbhelper);
    //jika load
    if(option.command=='load') load(dbhelper);
}

function helpvalidate(option, arg){
    if(option==null||option.length<1||arg.length<3) {
        console.log(`Please input correct param : There is no param found, type --help for help`)
        return false
    }
    if(option.help) {
        console.log(`Param available:
    --cmd {dump/load}    for loading or dumping to database
    --file {dbfilename}  specify databse name
    --init               for initiate database
    --test               for testing database
    --help               if you want help`)
        return false
    }
    if(option.file==null){
        console.log(`Please input correct param : --file {filename} param not found`)
        return false
    }
    return true

}

async function dump(dbhelper, init){
    var connectOptions = config.get('BrokerConnection');
    var connectionManager = new stompit.ConnectFailover(connectOptions);

    ////////////////////Failover
    var channel = new stompit.Channel(connectionManager);
    log.info('Success create new Channel')

    channel.subscribe(config.get('subscribeHeader').destination, function(error, message){
        if (error) {
            log.error('connect error ' + error.message);
            return;
        }
        message.readString('utf-8', function(error, body) {
            if (error) {
                log.error('read message error ' + error.message);
                return;
            }

            //header prep
            let idxmethod = message.headers.idxmethod?message.headers.idxmethod:'';
            let idxtotal = message.headers.idxtotal?message.headers.idxtotal:'';
            let idxnumber = message.headers.idxnumber?message.headers.idxnumber:'';
            let idxsender = message.headers.idxsender?message.headers.idxsender:'';
            let JMSXGroupId = message.headers.JMSXGroupId?message.headers.JMSXGroupId:'';
            let timestamp = new Date();
            let insertbody = Buffer.from(body).toString('base64');

            //dump to sqlite
            log.info('received message: ' + JMSXGroupId);
            dbhelper.db.run(`INSERT INTO Messaging_Data
            (idxmethod, idxtotal, idxnumber, idxsender, groupid, header, body, "timestamp")
            VALUES('${idxmethod}', '${idxtotal}', '${idxnumber}', '${idxsender}', '${JMSXGroupId}', '${JSON.stringify(message.headers)}', '${insertbody}', '${timestamp}');`,(err)=>{
                if (err) log.warn(`error inserting ${JMSXGroupId} to databse, please init your databse first`)
                log.info(`message reveieved & inserted from ${JMSXGroupId}`);
                resolve('')
            })
        });
        
    });
}

async function load(dbhelper){
    var sendto = config.get('sendDestination')
    var connectOptions = config.get('BrokerConnection');
    //var connectionManager = new stompit.ConnectFailover(connectOptions);
    
    ////////////////////Failover
    //var channel = new stompit.Channel(connectionManager);
    log.info('Success create new Channel')
    dbhelper.db.all(`select * from Messaging_Data;`, async (err, rows)=>{
        if(err){
            log.info('your database cannot be used')
        }
        for(var i = 0; i< rows.length; i++){
            for(var n = 0; n< sendto.length; n++){
                let header = JSON.parse(rows[i].header)
                let body = Buffer.from(rows[i].body, 'base64');
                header.destination = sendto[n]
                if(header['content-length']) {
                    header['content-length'] = body.length
                    header['content-type'] = 'application/binary;charset=utf-8'
                } else{
                    header['content-type'] = 'application/xml;charset=utf-8'
                }
                
                await new Promise(resolve=>{
                    try{
                        stompit.connect(connectOptions[0], function(error, client) {
                            if (error) {
                                console.log('connect error ' + error.message);
                                return;
                            }
                            client.on('error', function(error) {

                                // calling reconnect is optional and you may not want to reconnect if the
                                // same error will be repeated.
                                
                                log.warn('failed to connect: ', error, ' failed at ', new Date().toJSON());
                                //reconnect();
                            });
                            const frame = client.send(header);
                            try{
                                frame.write(body);
                                frame.end();
                                client.disconnect();
                            }catch (e){
                                console.log(e);
                            }
                            
                            //client.destroy();
                            console.log(`id: ${rows[i].id}, method: ${rows[i].idxmethod}, sender: ${rows[i].idxsender}`);
                            resolve();
                        });
                    }catch(e){
                        console.log(e);
                        resolve();
                    }
                    
                })
            }
        }
    })

}

main();