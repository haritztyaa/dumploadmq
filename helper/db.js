var sqlite3 = require('sqlite3').verbose();
var log = require('./logger');


class DbHelper {
    constructor (file='data.sqlite'){
        this.db = new sqlite3.Database(file);
    }
    init() {
        this.db.serialize(()=>{
            this.db.run(`drop TABLE if EXISTS Messaging_Data;`)
            this.db.run(`CREATE TABLE if not EXISTS Messaging_Data (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                "idxmethod" TEXT,
                idxtotal INTEGER,
                "idxnumber" INTEGER,
                idxsender TEXT,
                groupid TEXT,
                header BLOB,
                body BLOB,
                "timestamp" TEXT
            );
            CREATE INDEX Messaging_Data_id_IDX ON Messaging_Data (id,"timestamp",idxmethod,idxsender,groupid);`)
        })
    }
    close(){
        this.db.close();
    }
    async test(){
        return new Promise(async(resolve,reject)=>{
            for (var i = 0; i < 10; i++) {
                await new Promise((resolve,reject)=>{
                    this.db.run(`INSERT INTO Messaging_Data
                    (idxmethod, idxtotal, idxnumber, idxsender, groupid, header, body, "timestamp")
                    VALUES('testmethod', 3, 1, 'testsender', 'group01', 'jsdm,mvjjjdjsjd,sdjdjaj,cnnsjdfjj', 'dsjksjdcjdnvskdjfsdnj cjdnfsm,', 'now');`, (err)=>{
                        if(err) reject('Cannot insert to your database, this might because of incompability of data structure')
                        resolve('')
                    })
                })
            }
            await new Promise((resolve,reject)=>{
                this.db.each(`select * from Messaging_Data;`, (err, row)=>{
                    if(err){
                        console.log('your database cannot be used')
                        log.info('your database cannot be used')
                    }
                    console.log(`id: ${row.id}, method: ${row.idxmethod}, sender: ${row.idxsender}`);
                }, (err)=>{
                    if (err) reject('error insert')
                    resolve('')
                })
            }).then(()=>{
                this.db.run(`delete from messaging_data where idxsender = 'testsender'`,(err)=>{
                    if(err) log.info('cannot delete data test')
                    resolve('')
                })
            }).catch((reason)=>reject(reason))
        })
    }
}

// var db = new DbHelper('sqlite.sqlite')
// //db.init();
// //db.test();
// db.db.each(`select * from Messaging_Data;`, (err, row)=>{
//     if(err){
//         console.log('your database cannot be used')
//     }
//     console.log(`id: ${row.id}, method: ${row.idxmethod}, sender: ${row.idxsender}`);
// })

module.exports = {DbHelper}