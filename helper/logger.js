const config = require('config');
var log = require('simple-node-logger').createSimpleLogger('log.log');
log.setLevel(config.get('LogLevel'));

module.exports = log