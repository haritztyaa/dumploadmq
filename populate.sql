drop TABLE if EXISTS Messaging_Data;
CREATE TABLE if not EXISTS Messaging_Data (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	"idxmethod" TEXT,
	idxtotal INTEGER,
	"idxnumber" INTEGER,
	idxsender TEXT,
	groupid TEXT,
	header BLOB,
	body BLOB,
	"timestamp" TEXT
);
CREATE INDEX Messaging_Data_id_IDX ON Messaging_Data (id,"timestamp",idxmethod,idxsender,groupid);


INSERT INTO Messaging_Data
(idxmethod, idxtotal, idxnumber, idxsender, groupid, header, body, "timestamp")
VALUES('testmethod', 3, 1, 'idxnet', 'group01', 'jsdm,mvjjjdjsjd,sdjdjaj,cnnsjdfjj', 'dsjksjdcjdnvskdjfsdnj cjdnfsm,', 'now');

select * from Messaging_Data;

delete from Messaging_Data;